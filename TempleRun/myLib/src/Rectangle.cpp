#include <cmath>
#include <vector>
#include <iostream>
#include "myLib/common.hpp"
#include "myLib/Rectangle.hpp"

namespace myLib {

void Rectangle::build(const GLfloat& height, const GLfloat& width, const GLfloat& depth) {

    static const GLfloat position[] = {

        // face avant
        -width, -height, 0, //0
        width, -height, 0, //1
        width,  height, 0, //2

        width,  height, 0, //2
        -width,  height, 0, //3
        -width, -height, 0, //0
       
    };

    static const GLfloat normale[] = {

        // face avant
        0, 0, -1, //0
        0, 0, -1, //1
        0, 0, -1, //2
        0, 0, -1, //3
        0, 0, -1, //2
        0, 0, -1, //3
       
    };

    static const GLfloat texture[] = {

        // face avant
        
        0, 1, 9, //0
        1, 1, 9, //1
        1, 0, 9, //2

        1, 0, 9, //2
        0, 0, 9, //3        
        0, 1, 9, //0
    };


    std::vector<ShapeVertex> data;
    
    // Construit l'ensemble des vertex
    for(GLsizei i = 0; i <= 18; i+=3) {
        ShapeVertex vertex;

        vertex.position.x = position[i];
        vertex.position.y = position[i+1];
        vertex.position.z = position[i+2];

        vertex.normal.x = normale[i];
        vertex.normal.y = normale[i+1];
        vertex.normal.z = normale[i+2];

        vertex.texCoords.x = texture[i];
        vertex.texCoords.y = texture[i+1];
            
        data.push_back(vertex);
    }

    _nVertexCount = 6;

    for(GLsizei i = 0; i < _nVertexCount; ++i) {
        _vertices.push_back(data[i]);
    }

}

}
