#include "myLib/TrackballCamera.hpp"

namespace myLib
{

    TrackballCamera::TrackballCamera()
        : m_fDistance(-5.0f),
          m_fAngleX(-45.0f),
          m_fAngleY(0.0f),
          _isActive(false)
    {
    }

    // Avancer / reculer la caméra
    void TrackballCamera::moveFront(const float &delta)
    {
        m_fDistance += delta;
    }

    // Tourner latéralement autour du centre de vision
    void TrackballCamera::rotateLeft(const float &degrees)
    {
        m_fAngleX += degrees;
    }

    // Tourner verticalement autour du centre de vision
    void TrackballCamera::rotateUp(const float &degrees)
    {
        m_fAngleY += degrees;
    }

    bool TrackballCamera::getIsActive() const
    {
        return _isActive;
    }

    void TrackballCamera::active()
    {
        _isActive = true;
    }

    void TrackballCamera::disactive()
    {
        _isActive = false;
    }

    void TrackballCamera::reset()
    {
        m_fDistance = -5.0f;
        m_fAngleX = -45.0f;
        m_fAngleY = 0.0f;
    }

    // Calculer la ViewMatrix de la caméra
    glm::mat4 TrackballCamera::getViewMatrix() const
    {
        glm::mat4 viewMatrix = glm::mat4();
        viewMatrix = glm::translate(viewMatrix, glm::vec3(0, 0, m_fDistance));
        viewMatrix = glm::rotate(viewMatrix, glm::radians(m_fAngleX), glm::vec3(1, 0, 0));
        viewMatrix = glm::rotate(viewMatrix, glm::radians(m_fAngleY), glm::vec3(0, 0, 1));
        return viewMatrix;
    }
}