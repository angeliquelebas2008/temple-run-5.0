#include "myLib/Generation.hpp"

void drawMap(const Terrain &vecTerrain, const glm::mat4 &globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
             const Texture &floorTexture, const Texture &wallTexture, const float &time, const Texture &limitTexture)
{
    glm::mat4 cubeMVMatrix;
    int obstacle, coinValue = 0;
    for (size_t i = 0; i < vecTerrain.size() / 3; i++)
    {
        drawLimit(vecTerrain, globalMVMatrix, drawing, projMatrix, limitTexture, time, i);
        for (size_t j = 0; j < 3; j++)
        {
            cubeMVMatrix = glm::translate(globalMVMatrix, glm::vec3(j, i, 0)); // On se positionne à (i,j)
            obstacle = vecTerrain.getObstacle(i, j);                           // obstacle à dessiner
            coinValue = vecTerrain.getPieceValue(i, j);                        // valeur de la pièce de la même cellule
            // Si ce n'est pas un trou
            if (obstacle != 1)
            {
                drawing.draw(floorTexture, cubeMVMatrix, projMatrix, time);
                // Arche
                if (obstacle == 2)
                {
                    glm::mat4 archeMVmatrix = glm::scale(glm::translate(cubeMVMatrix, glm::vec3(0, 0, 1.5)), glm::vec3(1, 0.5, 0.5));
                    drawing.draw(wallTexture, archeMVmatrix, projMatrix, time);
                }
                // Mur
                else if (obstacle == 3)
                {
                    glm::mat4 murMVmatrix = glm::scale(glm::translate(cubeMVMatrix, glm::vec3(0, 0, 1.5)), glm::vec3(1, 0.5, 2));
                    drawing.draw(wallTexture, murMVmatrix, projMatrix, time);
                }
            }
        }
    }
    drawEndMap(vecTerrain, globalMVMatrix, drawing, projMatrix, limitTexture, time);
}

void drawLimit(const Terrain &vecTerrain, const glm::mat4 &globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
               const Texture &limitTexture, const float &time, const int &y)
{
    // Dessin des barrières sur les côtés
    glm::mat4 limitLeft = glm::scale(glm::translate(globalMVMatrix, glm::vec3(-1, y, 1)), glm::vec3(1, 1, 3));
    drawing.draw(limitTexture, limitLeft, projMatrix, time);

    glm::mat4 limitRight = glm::scale(glm::translate(globalMVMatrix, glm::vec3(3, y, 1)), glm::vec3(1, 1, 3));
    drawing.draw(limitTexture, limitRight, projMatrix, time);
}

void drawEndMap(const Terrain &vecTerrain, const glm::mat4 &globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
                const Texture &limitTexture, const float &time)
{
    int lastPosition = vecTerrain.size() / 3;
    // Dessin de trois murs à la fin du parcours
    glm::mat4 endWallLeft = glm::scale(glm::translate(globalMVMatrix, glm::vec3(0, lastPosition, 1)), glm::vec3(1, 1, 3));
    drawing.draw(limitTexture, endWallLeft, projMatrix, time);

    glm::mat4 endWallRight = glm::scale(glm::translate(globalMVMatrix, glm::vec3(2, lastPosition, 1)), glm::vec3(1, 1, 3));
    drawing.draw(limitTexture, endWallRight, projMatrix, time);

    glm::mat4 endWallMiddle = glm::scale(glm::translate(globalMVMatrix, glm::vec3(1, lastPosition, 1)), glm::vec3(1, 1, 3));
    drawing.draw(limitTexture, endWallMiddle, projMatrix, time);
}

glm::mat4 coinMatrix(const int &coinValue, const glm::mat4 &sphereMVMatrix)
{
    if (coinValue == 100)
    {
        return glm::scale(sphereMVMatrix, glm::vec3(0.25, 0.25, 0.25));
    }
    else if (coinValue == 250)
    {
        return glm::scale(sphereMVMatrix, glm::vec3(0.5, 0.5, 0.5));
    }
    return glm::scale(sphereMVMatrix, glm::vec3(1, 1, 1));
}

void drawCoins(const Terrain &vecTerrain, const glm::mat4 globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
               const Texture &texture, const float &time)
{
    glm::mat4 sphereMVMatrix;
    int obstacle, coinValue = 0;
    for (size_t i = 0; i < vecTerrain.size() / 3; i++)
    {
        for (size_t j = 0; j < 3; j++)
        {
            // On se positionne à (i,j)
            sphereMVMatrix = glm::translate(globalMVMatrix, glm::vec3(j, i, 1));
            obstacle = vecTerrain.getObstacle(i, j);
            coinValue = vecTerrain.getPieceValue(i, j);
            // Si ce n'est pas un mur et qu'elle a une valeur
            if (obstacle != 3 && coinValue != 0)
            {
                drawing.draw(texture, coinMatrix(coinValue, sphereMVMatrix), projMatrix, time);
            }
        }
    }
}