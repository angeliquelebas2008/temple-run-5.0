#include <myLib/EyeCamera.hpp>

namespace myLib
{

    EyeCamera::EyeCamera()
        : _position(0.0f, 0.0f, 0.0f),
          _fPhi(M_PI),
          _fTheta(M_PI),
          _isActive(true)
    {
        computeDirectionVectors();
    }

    void EyeCamera::rotateLeft(const float degrees)
    {
        float new_fPhi = _fPhi + glm::radians(degrees);
        if (M_PI * 0.5 < new_fPhi && new_fPhi < M_PI * 1.5)
        {
            _fPhi = new_fPhi;
        }
        computeDirectionVectors();
    }

    void EyeCamera::rotateUp(const float degrees)
    {
        float new_fTheta = _fTheta + glm::radians(degrees);
        if (M_PI * 0.5 < new_fTheta && new_fTheta < M_PI * 1.5)
        {
            _fTheta = new_fTheta;
        }
        computeDirectionVectors();
    }

    bool EyeCamera::getIsActive() const
    {
        return _isActive;
    }

    void EyeCamera::active()
    {
        _isActive = true;
    }

    void EyeCamera::disactive()
    {
        _isActive = false;
    }

    void EyeCamera::reset()
    {
        _position = glm::vec3(0.0f, 0.0f, 0.0f);
        _fPhi = M_PI;
        _fTheta = M_PI;
        computeDirectionVectors();
    }

    glm::mat4 EyeCamera::getViewMatrix(const Player &joueur)
    {
        _position = joueur.getPosition() + glm::vec3(-1, 1.5, 1.5);
        _position.y = 1.5;
        if (joueur.squatting())
        {
            _position.z = 0.8;
        }
        return glm::lookAt(_position, _position + _frontVector, _upVector);
    }

    void EyeCamera::computeDirectionVectors()
    {
        _frontVector = glm::vec3(cos(_fTheta) * sin(_fPhi),
                                 cos(_fTheta) * cos(_fPhi),
                                 sin(_fTheta));
        _leftVector = glm::vec3(sin(_fPhi + (M_PI * 0.5)),
                                cos(_fPhi + (M_PI * 0.5)),
                                0);
        _upVector = glm::cross(_frontVector, _leftVector);
    }

}