#include <myLib/Texture.hpp>

using namespace myLib;

Texture::Texture(const FilePath &path, const char *image)
{
    // load the image
    std::unique_ptr<Image> textureImage = loadImage(path.dirPath() + "assets/textures/" + image);
    if (!image)
    {
        std::cerr << "erreur de chargement de l'image" << std::endl;
    }

    // texture generation
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureImage->getWidth(), textureImage->getHeight(), 0, GL_RGBA, GL_FLOAT, textureImage->getPixels());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint Texture::get() const{
    return _texture;
}


