#include <myLib/Player.hpp>

namespace myLib
{

    unsigned int Player::getNbPoints() const
    {
        return _score;
    }

    glm::vec3 Player::getPosition() const
    {
        return _position;
    }

    glm::vec3 Player::getScale() const
    {
        return _scale;
    }
    void Player::increaseScore(const unsigned int &_coinValue)
    {
        _score += _coinValue;
    }

    //On réinitialise toutes les valeurs du joueurs pour commencer
    void Player::start()
    {
        _position.x = 1.0;
        _position.y = 0.5;
        _position.z = 0.0;
        _score = 0;
        _scale.x = 0.5;
        _scale.y = 0.5;
        _scale.z = 1.0;
    }

    //Pour avancer selon l'axe y en fonction de la translation du terrain
    void Player::caseAdvancement(const float &advancement)
    {   
        //On calcule la valeur entière supérieure à la translation plus 0.5 qui correspond à la profondeur du joueur
        float playerCase = ceil(-advancement + 0.5);
        //Si cette valeur qui correspond à la case du joueur dans le main est supérieur à celle de la classe joueur, on l'actualise
        if (playerCase > _position.y)
        {
            _position.y = playerCase;
        }
    }

    void Player::leftShift()
    {
        //On vérifie qu'on ne veut pas dépasser la limite gauche du terrain
        if (_position.x > 0)
        {
            _position.x -= 1;
        }
    }

    void Player::rightShift()
    {
        //On vérifie qu'on ne veut pas dépasser la limite droite du terrain
        if (_position.x < 2)
        {
            _position.x += 1;
        }
    }

    bool Player::inFlight() const
    {
        if (_position.z > 0)
        {
            return true;
        }
        else
            return false;
    }

    bool Player::squatting() const
    {
        if (_scale.z < 1.0)
        {
            return true;
        }
        return false;
    }

    void Player::jump()
    {
        //On ne peut sauter que si on est au sol sans glisser
        if (!inFlight() && !squatting())
        {
            _position.z = 1.0;
        }
    }

    void Player::landing()
    {
        _position.z = 0.;
    }

    void Player::slip()
    {
        //On ne peut glisser que si on est au sol sans être en action
        if (!inFlight() && !squatting())
        {
            _scale.z = 0.3;
        }
    }

    void Player::rising()
    {
        _scale.z = 1.0;
    }

    float Player::endMovement(float &actionDuration)
    {   
        //On met une limite de durée du saut et de la glissade
        if (actionDuration < 2.0f)
        {
            actionDuration += 0.08f;
            return actionDuration;
        }
        else
        {
            if (inFlight())
            {
                landing();
            }
            else
                rising();
            return 0;
        }
    }
};
