#include "myLib/Tile.hpp"

namespace myLib {

void Tile::build(const GLfloat& height, const GLfloat& width, const GLfloat& depth) {

    static const GLfloat vertices[] = {
        // face avant
        -width, -height, -depth, 0, 0, -1, 1, 1,
        width,  -height, -depth, 0, 0, -1, 0, 1,
        width,  height,  -depth, 0, 0, -1, 0, 0,

        width,   height, -depth, 0, 0, -1, 0, 0,
        -width,  height, -depth, 0, 0, -1, 1, 0,
        -width, -height, -depth, 0, 0, -1, 1, 1,

        // face dessus
        -width, height, -depth, 0, 1, 0, 1, 1, 
        width,  height, -depth, 0, 1, 0, 0, 1,
        width,  height,  depth, 0, 1, 0, 0, 0,

        width,  height,  depth, 0, 1, 0, 0, 0,
        -width, height,  depth, 0, 1, 0, 1, 0,
        -width, height, -depth, 0, 1, 0, 1, 1,

        // face côté gauche
        -width,  height,  depth, -1, 0, 0, 1, 0,
        -width,  height, -depth, -1, 0, 0, 0, 0,
        -width, -height, -depth, -1, 0, 0, 0, 1,

        -width, -height, -depth, -1, 0, 0, 0, 1,
        -width, -height,  depth, -1, 0, 0, 1, 1,
        -width,  height,  depth, -1, 0, 0, 1, 0,

        // face dessous
        -width, -height,  depth, 0, -1, 0, 1, 1,
        -width, -height, -depth, 0, -1, 0, 1, 0,
        width,  -height, -depth, 0, -1, 0, 0, 0,

        width,  -height, -depth, 0, -1, 0, 0, 0,
        -width, -height,  depth, 0, -1, 0, 1, 1,
        width,  -height,  depth, 0, -1, 0, 0, 1,
        
        // face arrière
        width,  -height, depth, 0, 0, 1, 1, 1,
        -width, -height, depth, 0, 0, 1, 0, 1,
        -width,  height, depth, 0, 0, 1, 0, 0,

        -width, height, depth, 0, 0, 1, 0, 0,
        width, -height, depth, 0, 0, 1, 1, 1,
        width,  height, depth, 0, 0, 1, 1, 0,

        // face côté droit
        width, -height, -depth, 1, 0, 0, 1, 1,
        width, -height,  depth, 1, 0, 0, 0, 1,
        width,  height,  depth, 1, 0, 0, 0, 0,

        width,  height,  depth, 1, 0, 0, 0, 0,
        width, -height, -depth, 1, 0, 0, 1, 1,
        width,  height, -depth, 1, 0, 0, 1, 0,
    };
  
    std::vector<ShapeVertex> data;
    
    // Construit l'ensemble des vertex
    for(GLsizei i = 0; i <= 288; i+=8) {
        ShapeVertex vertex;

        vertex.position.x = vertices[i];
        vertex.position.y = vertices[i+1];
        vertex.position.z = vertices[i+2];

        vertex.normal.x = vertices[i+3];
        vertex.normal.y = vertices[i+4];
        vertex.normal.z = vertices[i+5];

        vertex.texCoords.x = vertices[i+6];
        vertex.texCoords.y = vertices[i+7];
            
        data.push_back(vertex);
    }

    _nVertexCount = 36;

    for(GLsizei i = 0; i < _nVertexCount; ++i) {
        _vertices.push_back(data[i]);
    }

}

}
