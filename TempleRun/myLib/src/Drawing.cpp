#include <myLib/Drawing.hpp>

Drawing::Drawing(const FilePath &chemin, const char *shadersVS, const char *shadersFS, Object &object)
    : _program(loadProgram(chemin.dirPath() + "shaders/" + shadersVS,
                           chemin.dirPath() + "shaders/" + shadersFS)),
      _object(object)
{
    _MVPMatrix = glGetUniformLocation(_program.getGLId(), "uMVPMatrix");
    _MVMatrix = glGetUniformLocation(_program.getGLId(), "uMVMatrix");
    _normalMatrix = glGetUniformLocation(_program.getGLId(), "uNormalMatrix");
    _texture = glGetUniformLocation(_program.getGLId(), "uTexture");
    _lightDir = glGetUniformLocation(_program.getGLId(), "uLightDir_vs");

    if (shadersFS == "pieces.fs.glsl")
    {
        _lightPoint = glGetUniformLocation(_program.getGLId(), "uLightPos_vs");
    }

    loadVbo(_object);
    loadVao();
    sendVertex();
};

void Drawing::loadVao()
{
    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);
}

void Drawing::loadVbo(Object &object)
{
    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, object.getVertexCount() * sizeof(ShapeVertex), object.getDataPointer(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Drawing::sendVertex()
{
    const GLuint VERTEX_ATTR_POSITION = 0;
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    const GLuint VERTEX_ATTR_NORMALE = 1;
    glEnableVertexAttribArray(VERTEX_ATTR_NORMALE);
    const GLuint VERTEX_ATTR_TEXCOORD = 2;
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORD);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid *)offsetof(ShapeVertex, position));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid *)offsetof(ShapeVertex, normal));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid *)offsetof(ShapeVertex, texCoords));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Drawing::draw(const Texture& texture, const glm::mat4& globalMVMatrix, const glm::mat4& projMatrix, const float& time)
{
    glBindVertexArray(_vao);
    _program.use();
    glUniform1i(_texture, 0);

    glUniformMatrix4fv(_MVMatrix, 1, GL_FALSE, glm::value_ptr(globalMVMatrix));
    glUniformMatrix4fv(_normalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(globalMVMatrix))));
    glUniformMatrix4fv(_MVPMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix * globalMVMatrix));

    glm::mat4 MVLightDir = glm::rotate(globalMVMatrix, time * 0.25f + 45, glm::vec3(1, 0, 0));
    glm::vec3 lightDir(glm::vec4(1.f, 1.f, 1.f, 0.f) * MVLightDir);
    glUniform3f(_lightDir, lightDir.x, lightDir.y, lightDir.z);

    glm::mat4 MVLightPos = glm::rotate(globalMVMatrix, 45.f, glm::vec3(1, 0, 0));
    glm::vec3 lightPos(glm::vec4(1.f, 1.f, 1.f, 0.f) * MVLightPos);
    glUniform3f(_lightPoint, lightPos.x, lightPos.y, lightPos.z);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture.get());
    glDrawArrays(GL_TRIANGLES, 0, _object.getVertexCount());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Drawing::draw(const GLuint& texture, const glm::mat4& globalMVMatrix, const glm::mat4 &projMatrix)
{
    glBindVertexArray(_vao);
    _program.use();
    glUniform1i(_texture, 0);

    glUniformMatrix4fv(_MVMatrix, 1, GL_FALSE, glm::value_ptr(globalMVMatrix));
    glUniformMatrix4fv(_normalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(globalMVMatrix))));
    glUniformMatrix4fv(_MVPMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix * globalMVMatrix));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glDrawArrays(GL_TRIANGLES, 0, _object.getVertexCount());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Drawing::drawPlayer(const Texture& texture, const glm::mat4& globalMVMatrix, const glm::mat4& projMatrix, const Player& player,
                         const float& time)
{
    glm::vec3 playerCoords = player.getPosition();
    glm::vec3 playerScale = player.getScale();
    glm::mat4 playerMVMatrix = glm::scale(glm::translate(globalMVMatrix, glm::vec3(playerCoords.x - 1, 0, 1 + playerCoords.z)), 
                                                         glm::vec3(playerScale.x, playerScale.y, playerScale.z));
    this->draw(texture, playerMVMatrix, projMatrix, time);
}

void Drawing::drawEnemy(const Texture& texture, const glm::mat4& globalMVMatrix, const glm::mat4& projMatrix, const Player& player, 
                        const float& time)
{
    glm::vec3 playerCoords = player.getPosition();
    glm::vec3 playerScale = player.getScale();
    glm::mat4 enemyMVMatrix = glm::scale(glm::translate(globalMVMatrix, glm::vec3(playerCoords.x - 1, -2, 1 + playerCoords.z)), 
                                                                        glm::vec3(playerScale.x, playerScale.y, playerScale.z));
    this->draw(texture, enemyMVMatrix, projMatrix, time);
}