#include <myLib/Collision.hpp>

bool faceCollision(myLib::Player &player, Terrain &vecTerrain)
{
    glm::vec3 coordsJoueur = player.getPosition();
    int obstacle = vecTerrain.get(coordsJoueur.y, coordsJoueur.x).getObstacle();
    // Collision s'il y a un mur
    if (obstacle == 3)
    {
        return true;
    }
    // Collision s'il y a une arche et que le joueur n'est pas squatting
    else if (obstacle == 2 && !player.squatting())
    {
        return true;
    }
    else
        return false;
}

bool fall(myLib::Player &player, Terrain &vecTerrain)
{
    glm::vec3 coordsJoueur = player.getPosition();
    int obstacle = vecTerrain.get(coordsJoueur.y, coordsJoueur.x).getObstacle();
    // Si il y a un trou et que le joueur n'est pas en hauteur
    if (obstacle == 1 && coordsJoueur.z <=0)
    {
        return true;    
    }
    return false;
}

bool leftSideCollision(myLib::Player &player, Terrain &vecTerrain)
{
    glm::vec3 coordsJoueur = player.getPosition();
    int obstacle = vecTerrain.get(coordsJoueur.y, coordsJoueur.x - 1).getObstacle();
    // Si l'obstacle est soit une arche soit un mur
    if (obstacle >= 2)
    {
        return true;
    }
    else return false;
}

bool rightSideCollison(myLib::Player &player, Terrain &vecTerrain)
{
    glm::vec3 coordsJoueur = player.getPosition();
    int obstacle = vecTerrain.get(coordsJoueur.y, coordsJoueur.x + 1).getObstacle();
    // Si l'obstacle est soit une arche soit un mur
    if (obstacle >= 2)
    {
        return true;
    }
    else return false;
}
