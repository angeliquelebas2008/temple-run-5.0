#include <myLib/Texte.hpp>

namespace myLib
{

    Text::Text(const FilePath &path, const char *shadersVS, const char *shadersFS, const char *message)
        : _drawing(path, shadersVS, shadersFS, _rectangle)
    {

        TTF_Init();
        if (TTF_Init() == -1)
        {
            fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
            exit(EXIT_FAILURE);
        }

        /* Chargement de la police */
        _font = TTF_OpenFont("./projet/assets/fonts/luciole.ttf", 65);
        try
        {
            if (!_font)
            {
                throw std::string(TTF_GetError());
            }
        }
        catch (const std::string &error)
        {
            std::cerr << "error : " << error << std::endl;
        }

        /* Écriture du texte dans la SDL_Surface texte en mode Blended (optimal) */
        // ici Shaded
        _text = TTF_RenderText_Shaded(_font, message, _whiteColor, _blackColor);
        try
        {
            if (!_text)
            {
                throw std::string(TTF_GetError());
            }
        }
        catch (const std::string &error)
        {
            std::cerr << "error : " << error << std::endl;
        }

        loadTexture();

        TTF_CloseFont(_font); /* Doit être avant TTF_Quit() */
        TTF_Quit();

        SDL_FreeSurface(_text);
    }

    void Text::loadTexture()
    {

        // Détermination du format
        GLint colors = _text->format->BytesPerPixel;
        GLint texture_format;
        if (colors == 4)
        { // alpha
            if (_text->format->Rmask == 0x000000ff)
                texture_format = GL_RGBA;
            else
                texture_format = GL_BGRA;
        }
        else
        { // no alpha
            if (_text->format->Rmask == 0x000000ff)
                texture_format = GL_RGB;
            else
                texture_format = GL_BGR;
        }

        // Génération de la texture
        glGenTextures(1, &_texture);
        glBindTexture(GL_TEXTURE_2D, _texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, colors, _text->w, _text->h, 0,
                     texture_format, GL_UNSIGNED_BYTE, _text->pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void Text::write(const glm::mat4 &globalMVMatrix, const glm::mat4 &projMatrix)
    {
        _drawing.draw(_texture, globalMVMatrix, projMatrix);
    }

    GLuint Text::getTexture() const
    {
        return _texture;
    }

}