#include "myLib/Terrain.hpp"

Terrain::Terrain(const std::string &fileName)
    : _vec()
{
    std::ifstream fichier(fileName);

    // test d'ouverture
    if (!fichier.is_open())
    {
        std::cout << "Can not open file: " << fileName << std::endl;
        return;
    }

    double type;
    fichier >> type;

    size_t xsize, ysize;
    fichier >> xsize;
    fichier >> ysize;
    size_t size = xsize * ysize;
    _vec.resize(size);

    size_t max;
    fichier >> max;

    std::function<int(const int &)> convertToObstacle;
    convertToObstacle = [](const int &val)
    {
        if (val % 2 == 0)
        {
            return 0; // sol sans obstacle
        }
        else if (val % 3 == 0)
        {
            return 1; // vide
        }
        else if (val % 5 == 0)
        {
            return 2; // arche
        }
        else
            return 3; // mur
    };

    int nb;
    int indiceObstacle;
    for (size_t i = 0; i < size; i++)
    {
        fichier >> nb;
        indiceObstacle = convertToObstacle(nb);
        _vec[i] = {indiceObstacle, convertToCoin(indiceObstacle)};
    }

    fichier.close();
}

Terrain::Terrain(const Terrain &v)
{
    _vec.resize(v.size());
}

Terrain& Terrain::operator=(const Terrain &v)
{
    if(&v == this){
        return *this; // on retourne l'object courant
    }
    _vec = v._vec;

    return *this;
}

const myLib::Cell &Terrain::operator[](const size_t &i) const
{
    return _vec[i];
}

myLib::Cell &Terrain::operator[](const size_t &i)
{
    return _vec[i];
}

std::ostream &operator<<(std::ostream &stream, const Terrain &v)
{
    if (v.size() == 0)
    {
        return stream;
    }
    stream << "(";

    for (size_t i = 0; i < v.size() - 1; i++)
    {
        stream << v[i].getCoinValue() << " , ";
    }

    stream << v[v.size() - 1].getCoinValue() << ")";
    return stream;
}

myLib::Cell Terrain::get(const size_t &i, const size_t &j) const
{
    return _vec[i * 3 + j];
};

int Terrain::getPieceValue(const size_t &i, const size_t &j) const
{
    return this->get(i, j).getCoinValue();
}

int Terrain::getObstacle(const size_t &i, const size_t &j) const
{
    return this->get(i, j).getObstacle();
}

void Terrain::deletePiece(const size_t &i, const size_t &j)
{
    _vec[i * 3 + j]._coinValue = 0;
}

int convertToCoin(const size_t& val)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distrib(0, 10);
    auto dice = std::bind(distrib, generator);

    std::function<int(const int &)> convertToValeur;
    convertToValeur = [](const int &indiceCoin)
    {
        if (indiceCoin > 7)
        {
            return 100;
        }
        else if (indiceCoin > 5)
        {
            return 250;
        }
        else if (indiceCoin > 4)
        {
            return 500;
        }
        else
            return 0;
    };

    if (val != 3)
    {
        return convertToValeur(dice());
    }
    else
        return 0;
}
