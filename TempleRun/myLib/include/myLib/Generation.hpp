#pragma once

#include <GL/glew.h>
#include <vector>
#include "common.hpp"
#include "Terrain.hpp"
#include "Drawing.hpp"

void drawMap(const Terrain &vecTerrain, const glm::mat4 &globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
             const Texture &floorTexture, const Texture &wallTexture, const float &time, const Texture &limitTexture);

void drawLimit(const Terrain &vecTerrain, const glm::mat4 &globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
               const Texture &limitTexture, const float &time, const int &y);

void drawEndMap(const Terrain &vecTerrain, const glm::mat4 &globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
                const Texture &limitTexture, const float &time);

glm::mat4 coinMatrix(const int &coinValue, const glm::mat4 &sphereMVMatrix);

void drawCoins(const Terrain &vecTerrain, const glm::mat4 globalMVMatrix, Drawing &drawing, const glm::mat4 &projMatrix,
               const Texture &texture, const float &time);