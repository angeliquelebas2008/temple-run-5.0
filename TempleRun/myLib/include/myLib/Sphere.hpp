#pragma once

#include <vector>
#include "common.hpp"
#include "Object.hpp"

namespace myLib
{
    // Représente une sphère discrétisée centrée en (0, 0, 0) (dans son repère local)
    // Son axe vertical est (0, 1, 0) et ses axes transversaux sont (1, 0, 0) et (0, 0, 1)
    class Sphere : public Object
    {

    public:
        // Constructor
        Sphere(const GLfloat &radius, const GLfloat &discLat, const GLfloat &discLong)
            : Object()
        {
            build(radius, discLat, discLong); // Construction (voir le .cpp)
        }

        // Destructor
        ~Sphere(){};

    protected:
        // Method
        void build(const GLfloat &height, const GLfloat &width, const GLfloat &depth) override;
    };

}