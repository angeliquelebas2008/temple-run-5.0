#pragma once

#include <vector>
#include <iostream>
#include <cmath>
#include <fstream>
#include <functional>
#include <chrono>
#include <random>
#include "Cell.hpp"

class Terrain
{

private:
    // Attributes
    std::vector<myLib::Cell> _vec;

public:
    // Constructors
    Terrain() : _vec()
    {
        for (size_t i = 0; i < _vec.size(); i++)
        {
            _vec[i] = {};
        }
    };
    Terrain(const std::string &fileName);
    Terrain(const Terrain &v);

    // Destructor
    ~Terrain() = default;

    // Methods
    void display() const;
    inline size_t size() const
    {
        return _vec.size();
    };
    void deletePiece(const size_t &i, const size_t &j);

    // Getter
    myLib::Cell get(const size_t &i, const size_t &j) const;
    int getPieceValue(const size_t &i, const size_t &j) const;
    int getObstacle(const size_t &i, const size_t &j) const;

    // Operators
    Terrain &operator=(const Terrain &v);
    const myLib::Cell &operator[](const size_t &i) const;
    myLib::Cell &operator[](const size_t &i);
};

std::ostream &operator<<(std::ostream &stream, const Terrain &v);
int convertToCoin(const size_t& val);
