#pragma once

#include <vector>
#include <iostream>
#include <string>
#include <GL/glew.h>
#include "glm.hpp"
#include "stb_image.h"
#include "shader_m.h"

class Skybox
{
private:
    // Attributes
    shader_m::Shader _skyboxShader;
    unsigned int _skyboxVAO, _skyboxVBO;
    unsigned int _cubemapTexture;

public:
    // Methods
    void initSkybox();
    void draw(const glm::mat4 &projMatrix, glm::mat4 cameraViewMatrix) const;
};

unsigned int loadCubemap(const std::vector<std::string> &faces);