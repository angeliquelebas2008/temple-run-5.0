#pragma once

#include <GL/glew.h>
#include "Program.hpp"
#include "Image.hpp"
#include <iostream>

namespace myLib
{

    class Texture
    {
    private:
        // Attributes
        GLuint _texture;

    public:
        // Constructor
        Texture(const FilePath &path, const char *image);

        // Destructor
        ~Texture() { glDeleteTextures(1, &_texture); }

        // Getter
        GLuint get() const;
    };

}