#pragma once

#include <cmath>
#include <vector>
#include "common.hpp"
#include "Object.hpp"

namespace myLib
{

    class Tile : public Object
    {

    public:
        // Constructor
        Tile(const GLfloat& height, const GLfloat& width, const GLfloat& depth)
            : Object()
        {
            build(height, width, depth); // Construction
        }

        ~Tile(){};

    protected:
        void build(const GLfloat& height, const GLfloat& width, const GLfloat& depth) override;
    };

}
