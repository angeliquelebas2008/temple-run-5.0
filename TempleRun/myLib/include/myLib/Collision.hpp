#pragma once

#include <vector>
#include "Player.hpp"
#include "Terrain.hpp"

bool faceCollision(myLib::Player &player, Terrain &vecTerrain);

bool fall(myLib::Player &player, Terrain &vecTerrain);

bool leftSideCollision(myLib::Player &player, Terrain &vecTerrain);

bool rightSideCollison(myLib::Player &player, Terrain &vecTerrain);
