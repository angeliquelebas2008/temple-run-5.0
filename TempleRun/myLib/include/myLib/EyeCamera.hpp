#pragma once

#include "glm.hpp"
#include "Player.hpp"

namespace myLib
{

    class EyeCamera
    {

    private:
        // Attributes
        glm::vec3 _position;    // position de la caméra
        float _fPhi;            // coordonées sphériques du vecteur F
        float _fTheta;          // coordonées sphériques du vecteur F
        glm::vec3 _frontVector; // vecteur F
        glm::vec3 _leftVector;  // vecteur L
        glm::vec3 _upVector;    // vecteur U
        bool _isActive;

    public:
        // Constructor
        EyeCamera();

        // Destructor
        ~EyeCamera() {}

        // Methods
        void rotateLeft(const float degrees);
        void rotateUp(const float degrees);
        void active();                                 // Active la caméra
        void disactive();                              // Désactive la caméra
        void reset();                                  // reset camera
        glm::mat4 getViewMatrix(const Player &player); // Calculer la ViewMatrix de la caméra

        // Getter
        bool getIsActive() const;

    private:
        void computeDirectionVectors();
    };

}