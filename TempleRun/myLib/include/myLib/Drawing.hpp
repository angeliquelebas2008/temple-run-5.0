#pragma once

#include "Program.hpp"
#include "Object.hpp"
#include "Texture.hpp"
#include "Player.hpp"

using namespace myLib;

class Drawing
{

private:
    // Attributes
    GLuint _vbo;
    GLuint _vao;
    Object &_object;

    void loadVao();
    void loadVbo(Object &object);
    void sendVertex();

public:
    Program _program;
    GLint _MVPMatrix;
    GLint _MVMatrix;
    GLint _normalMatrix;
    GLint _texture;
    GLint _lightDir;
    GLint _lightPoint;

    // Constructor
    Drawing(const FilePath &chemin, const char *shadersVS, const char *shadersFS, Object &object);

    // Destructor
    ~Drawing()
    {
        glDeleteBuffers(1, &_vbo);
        glDeleteVertexArrays(1, &_vao);
    }

    // Methods
    void draw(const Texture &texture, const glm::mat4 &globalMVMatrix, const glm::mat4 &projMatrix, const float &time);
    void draw(const GLuint &texture, const glm::mat4 &globalMVMatrix, const glm::mat4 &projMatrix);
    void drawPlayer(const Texture &texture, const glm::mat4 &globalMVMatrix, const glm::mat4 &projMatrix, const Player &player,
                    const float &time);
    void drawEnemy(const Texture &texture, const glm::mat4 &globalMVMatrix, const glm::mat4 &projMatrix, const Player &player,
                   const float &time);
};