#pragma once

#include "glm.hpp"
#include "Player.hpp"

namespace myLib
{

    class TrackballCamera
    {
    private:
        // Attributes
        float m_fDistance; // distance par rapport au centre de la scène
        float m_fAngleX;   // angle effectuée par la caméra autour de l'axe x de la scène (rotation vers le haut ou vers le bas)
        float m_fAngleY;   // angle effectuée par la caméra autour de l'axe y de la scène (rotation vers la gauche ou vers la droite)
        bool _isActive;

    public:
        // Constructor
        TrackballCamera();

        // Destructor
        ~TrackballCamera() {}

        // Methods
        void moveFront(const float &delta);    // Avancer / reculer la caméra
        void rotateLeft(const float &degrees); // Tourner latéralement autour du centre de vision
        void rotateUp(const float &degrees);   // Tourner verticalement autour du centre de vision
        glm::mat4 getViewMatrix() const;       // Calculer la ViewMatrix de la caméra
        void active();                         // Active la caméra
        void disactive();                      // Désactive la caméra
        void reset();

        // Getter
        bool getIsActive() const;
    };

}