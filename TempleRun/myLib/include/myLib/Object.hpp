#pragma once

#include <vector>
#include "common.hpp"

constexpr bool verbatim = false;

namespace myLib
{

    class Object
    {

    protected:
        // Attributes
        std::vector<ShapeVertex> _vertices;
        GLsizei _nVertexCount; // Nombre de sommets

    public:
        // Constructor
        Object() : _nVertexCount(0) {}
        Object(Object &object) : _vertices(object._vertices), _nVertexCount(object._nVertexCount){};

        // Destrcutor
        virtual ~Object(){};

        // Methods
        const ShapeVertex *getDataPointer() const
        {
            return &_vertices[0]; // Renvoit le pointeur vers les données
        }
        GLsizei getVertexCount() const
        {
            return _nVertexCount; // Renvoit le nombre de vertex
        }

    protected:
        // Method
        virtual void build(const GLfloat &height, const GLfloat &width, const GLfloat &depth) = 0;
    };

}
