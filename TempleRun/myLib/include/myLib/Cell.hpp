#pragma once

#include <vector>
#include "glm.hpp"

namespace myLib
{

   class Cell
   {

   private:
      // Attributes
      glm::vec3 _position;
      int _obstacle;

   public:
      unsigned int _coinValue;

      // Constructors
      Cell() : _coinValue(0), _position(0, 0, 0), _obstacle(0){};
      Cell(const int &idObstacle, const int &value) : _coinValue(value), _obstacle(idObstacle){};

      // Getters
      unsigned int getCoinValue() const
      {
         return _coinValue;
      }
      int getObstacle() const
      {
         return _obstacle;
      }

      // Setters
      void setCoinValue(const unsigned int &value)
      {
         _coinValue = value;
      }
      void setObstacle(const int &idObstacle)
      {
         _obstacle = idObstacle;
      }

      // Destructor
      ~Cell() = default;
   };

}