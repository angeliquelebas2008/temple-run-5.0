#pragma once

#include <vector>
#include "glm.hpp"

namespace myLib
{

    class Player
    {

    private:
        // Atrributes
        int _score;
        glm::vec3 _position;
        glm::vec3 _scale;

    public:
        // Constructor
        Player() : _score(0), _position(1., 0.5, 0.), _scale(0.5, 0.5, 1.0){};

        // Destructor
        ~Player(){};

        // Getters
        unsigned int getNbPoints() const;
        glm::vec3 getPosition() const;
        glm::vec3 getScale() const;

        // Setters
        void increaseScore(const unsigned int &coinValue);
        void start();
        void caseAdvancement(const float &advancement);
        void leftShift();
        void rightShift();
        bool inFlight() const;

        // Methods
        bool squatting() const;
        void jump();
        void landing();
        void slip();
        void rising();
        float endMovement(float &actionDuration);
    };

};