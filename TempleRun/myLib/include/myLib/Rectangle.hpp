#pragma once

#include <vector>
#include "common.hpp"
#include "Object.hpp"

namespace myLib
{

    class Rectangle : public Object
    {

    public:
        // Constructor
        Rectangle(const GLfloat &height, const GLfloat &width)
            : Object()
        {
            build(height, width, 0); // Construction
        }

        // Destructor
        ~Rectangle(){};

    protected:
        // Method
        void build(const GLfloat &height, const GLfloat &width, const GLfloat &depth) override; // Alloue et construit les données
    };

}
