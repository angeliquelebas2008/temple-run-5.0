#pragma once
#include <SDL/SDL_ttf.h>
#include <iostream>
#include <GL/glew.h>
#include "Rectangle.hpp"
#include "FilePath.hpp"
#include "Drawing.hpp"

namespace myLib
{

    class Text
    {
    private:
        // Atrributes
        SDL_Surface *_text = NULL;
        TTF_Font *_font = NULL;
        SDL_Color _blackColor = {0, 0, 0};
        SDL_Color _whiteColor = {255, 255, 255};
        GLuint _texture;
        Rectangle _rectangle = {1, 1};
        Drawing _drawing;

    public:
        // Constructor
        Text(const FilePath &path, const char *shadersVS, const char *shadersFS, const char *message);

        // Destructor
        ~Text(){};

        // Methods
        void loadTexture();                                                       // load the texture
        void write(const glm::mat4 &globalMVMatrix, const glm::mat4 &projMatrix); // display the text

        // Getter
        GLuint getTexture() const;
    };

}