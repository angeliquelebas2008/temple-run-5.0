#version 300 es

precision mediump float;

out vec4 FragColor;

in vec3 TexCoords;

uniform samplerCube skybox;
/*
void main(){
    FragColor = texture(skybox, TexCoords);
    FragColor = vec4(1.0, 0., 0., 1.);
}*/

//uniform sampler2D uTexture;

//out vec3 fFragTexture;

void main() {
    FragColor = texture(skybox, TexCoords);
    //FragColor = vec4(TexCoords, 1.);


    //fFragTexture = normalize(vNormal_vs);
}