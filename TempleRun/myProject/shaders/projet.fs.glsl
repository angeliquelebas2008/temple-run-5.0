#version 300 es

precision mediump float;

in vec3 vPosition_vs;
in vec3 vNormal_vs; 
in vec2 vTexCoords;

uniform sampler2D uTexture;
uniform vec3 uLightDir_vs;

out vec3 fFragTexture;


vec3 lumiereDirectionnelle(){

  vec3 diffuse = vec3(0.2f, 0.2f, 0.2f);
  vec3 glossy = vec3(0.2f, 0.2f, 0.2f);
  float shininess = 2.f;
  vec3 LightIntensity = vec3(0.8f, 0.8f, 0.6f);
  //vec3 LightIntensity = vec3(1.0f, 1.0f, 1.0f);

  vec3 directionLight = normalize(uLightDir_vs);
  vec3 directionCamera = normalize(-vPosition_vs);
  vec3 halfVector = (directionLight + directionCamera) / 2.0;
  return  LightIntensity * (diffuse * dot(directionCamera, vNormal_vs) + glossy* pow(dot(halfVector, vNormal_vs), shininess));
}


void main() {

  vec4 texture = texture(uTexture, vTexCoords);
  fFragTexture = vec3(texture)+lumiereDirectionnelle();
  
  //fFragTexture = normalize(vNormal_vs);
};