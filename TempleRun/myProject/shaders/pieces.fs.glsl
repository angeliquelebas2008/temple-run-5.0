#version 300 es

precision mediump float;

in vec3 vPosition_vs;
in vec3 vNormal_vs; 
in vec2 vTexCoords;

uniform sampler2D uTexture;
uniform vec3 uLightDir_vs;
uniform vec3 uLightPos_vs;

out vec3 fFragTexture;


vec3 lumiereDirectionnelle(){

  // paramètrage de la lumière
  vec3 diffuse = vec3(0.2f, 0.2f, 0.2f);
  vec3 glossy = vec3(0.2f, 0.2f, 0.2f);
  float shininess = 2.f;
  vec3 lightIntensity = vec3(0.8f, 0.8f, 0.6f);

  // calcul du vecteur final
  vec3 directionLight = normalize(uLightDir_vs);
  vec3 directionCamera = normalize(-vPosition_vs);
  vec3 halfVector = (directionLight + directionCamera) / 2.0;
  return  lightIntensity * (diffuse * dot(directionCamera, vNormal_vs) + glossy* pow(dot(halfVector, vNormal_vs), shininess));
}


vec3 lumierePonctuelle(){

  // paramètrage de la lumière
  vec3 diffuse = vec3(0.2f, 0.2f, 0.2f);
  vec3 glossy = vec3(0.2f, 0.2f, 0.2f);
  float shininess = 5.f;
  vec3 lightIntensity = vec3(0.1f, 0.1f, 0.0f);

  // calcul du vecteur final
  lightIntensity = lightIntensity/(distance(vPosition_vs, uLightPos_vs)*distance(vPosition_vs, uLightPos_vs));
  vec3 directionLight = normalize(uLightPos_vs - vPosition_vs);
  vec3 directionCamera = normalize(-vPosition_vs);
  vec3 halfVector = (directionCamera + directionLight) / 2.0;

  return  lightIntensity * (diffuse * dot(directionLight , vNormal_vs) + glossy * pow(dot(halfVector, vNormal_vs), shininess));
}

void main() {

  vec4 texture = texture(uTexture, vTexCoords);
  fFragTexture = vec3(texture)+lumierePonctuelle()+lumiereDirectionnelle();
  
  //fFragTexture = normalize(vNormal_vs);
};