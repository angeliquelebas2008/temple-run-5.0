#include <myLib/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include <myLib/TrackballCamera.hpp>
#include <myLib/EyeCamera.hpp>
#include <myLib/Tile.hpp>
#include <myLib/Terrain.hpp>
#include <myLib/Rectangle.hpp>
#include <myLib/FilePath.hpp>
#include <myLib/Generation.hpp>
#include <myLib/Texte.hpp>
#include <myLib/Player.hpp>
#include <myLib/Drawing.hpp>
#include <myLib/Texture.hpp>
#include <myLib/Collision.hpp>
#include <myLib/Sphere.hpp>
#include <myLib/Skybox.hpp>
#include <myLib/shader_m.h>
#include <math.h>

using namespace myLib;

int main(int argc, char **argv)
{

    /************** INITIALISATION *******************/

    // Initialize SDL and open a window
    SDLWindowManager windowManager(1200, 900, "Templeris");

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if (GLEW_OK != glewInitError)
    {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    FilePath applicationPath(argv[0]);

    // Création des textures
    Texture floorTexture(applicationPath, "sol.jpg");
    Texture tileTexture(applicationPath, "orange.png");
    Texture limitTexture(applicationPath, "mur.jpg");
    Texture playerTexture(applicationPath, "pink.png");
    Texture enemyTexture(applicationPath, "yellow.png");
    Texture coinTexture(applicationPath, "piece.jpg");
    Texture menuTexture(applicationPath, "menu5.jpg");
    Texture pauseTexture(applicationPath, "pause.jpg");
    Texture gameOverTexture(applicationPath, "game_over.jpg");

    Skybox skybox = Skybox();
    skybox.initSkybox();

    glEnable(GL_DEPTH_TEST);

    // Préparation des matrices
    glm::mat4 projMatrix = glm::perspective(glm::radians(70.f), 1200 / 900.f, 0.1f, 100.f);
    glm::mat4 MVMatrix = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5));
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    // Caméra
    TrackballCamera cameraTrackExplorateur;
    EyeCamera cameraEyeExplorateur;

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    /************** BOUCLE DE RENDU *******************/

    // Remplissage du vec du terrain
    std::string fileTxt("./myProject/assets/parcours/parcours.txt");
    Terrain vecTerrain(fileTxt);

    Tile tile(0.5, 0.5, 0.5);
    Tile ground(1, 1, 1);
    Sphere coin(0.25, 32, 16);
    Player player;
    Tile menu(1.5, 2., 0.5);
    float advancement = 0;
    float actionDuration = 0;
    bool pause = true;
    bool game = false;
    bool gameOver = false;

    // Création des programmes
    Drawing floorDrawing(applicationPath, "projet.vs.glsl", "projet.fs.glsl", ground);
    Drawing tileDrawing(applicationPath, "projet.vs.glsl", "projet.fs.glsl", tile);
    Drawing playerDrawing(applicationPath, "projet.vs.glsl", "projet.fs.glsl", tile);
    Drawing coinDrawing(applicationPath, "projet.vs.glsl", "pieces.fs.glsl", coin);
    Drawing menuDrawing(applicationPath, "projet.vs.glsl", "pieces.fs.glsl", menu);

    /*********************************/

    // Application loop:
    bool done = false;
    while (!done)
    {
        // Event loop:
        SDL_Event e;
        while (windowManager.pollEvent(e))
        {
            switch ((e.type))
            {
            case SDL_QUIT:
                done = true; // Leave the loop after this iteration
                break;

            case SDL_KEYDOWN:
                if (windowManager.isKeyPressed(SDLK_UP) && cameraTrackExplorateur.getIsActive())
                {
                    cameraTrackExplorateur.moveFront(0.5f);
                }
                if (windowManager.isKeyPressed(SDLK_DOWN) && cameraTrackExplorateur.getIsActive())
                {
                    cameraTrackExplorateur.moveFront(-0.5);
                }
                // Bouton start
                if (windowManager.isKeyPressed(SDLK_SPACE))
                {
                    if (!game)
                    {
                        advancement = 0;
                        pause = false;
                        game = true;
                    }
                }
                if (game)
                {
                    // Bouton pause
                    if (windowManager.isKeyPressed(SDLK_ESCAPE))
                    {
                        if (!pause)
                        {
                            if (cameraTrackExplorateur.getIsActive())
                            {
                                cameraTrackExplorateur.disactive();
                                cameraEyeExplorateur.active();
                            }
                            pause = true;
                        }
                        else
                            pause = false;
                    }
                    // Retour au départ
                    if (windowManager.isKeyPressed(SDLK_r))
                    {
                        if (pause || gameOver)
                        {
                            game = false;
                            advancement = 0;
                            player.start();
                            pause = true;
                            gameOver = false;
                        }
                    }
                    if (!pause)
                    {
                        // Déplacement à gauche
                        if (windowManager.isKeyPressed(SDLK_q))
                        {
                            if (!leftSideCollision(player, vecTerrain) && player.getPosition().x != 0)
                            {
                                player.leftShift();
                            }
                        }
                        // Déplacement à droite
                        if (windowManager.isKeyPressed(SDLK_d))
                        {
                            if (!rightSideCollison(player, vecTerrain) && player.getPosition().x != 2)
                            {
                                player.rightShift();
                            }
                        }
                        // Saut
                        if (windowManager.isKeyPressed(SDLK_z))
                        {
                            actionDuration = 0;
                            player.jump();
                        }
                        // Glissade
                        if (windowManager.isKeyPressed(SDLK_s))
                        {
                            actionDuration = 0;
                            player.slip();
                        }
                        // Changement caméra
                        if (windowManager.isKeyPressed(SDLK_c))
                        {
                            if (cameraTrackExplorateur.getIsActive())
                            {
                                cameraTrackExplorateur.disactive();
                                cameraEyeExplorateur.active();
                            }
                            else
                            {
                                cameraTrackExplorateur.active();
                                cameraEyeExplorateur.disactive();
                            }
                        }
                        // Reset caméra
                        if (windowManager.isKeyPressed(SDLK_l))
                        {
                            if (cameraTrackExplorateur.getIsActive())
                            {
                                cameraTrackExplorateur.reset();
                            }
                            else
                            {
                                cameraEyeExplorateur.reset();
                            }
                        }
                    }
                }

            case SDL_MOUSEMOTION:
                if (game)
                {
                    if (windowManager.isMouseButtonPressed(SDL_BUTTON_LEFT) && cameraTrackExplorateur.getIsActive())
                    {
                        glm::vec2 posMouse = glm::vec2(e.motion.xrel, e.motion.yrel);
                        if (posMouse.x != 0)
                        {
                            cameraTrackExplorateur.rotateUp(posMouse.x);
                        }
                        if (posMouse.y != 0)
                        {
                            cameraTrackExplorateur.rotateLeft(posMouse.y);
                        }
                    }
                    if (windowManager.isMouseButtonPressed(SDL_BUTTON_RIGHT) && cameraEyeExplorateur.getIsActive())
                    {
                        glm::vec2 posMouse = glm::vec2(e.motion.xrel, e.motion.yrel);
                        if (posMouse.x != 0)
                        {
                            cameraEyeExplorateur.rotateLeft(posMouse.x);
                        }
                        if (posMouse.y != 0)
                        {
                            cameraEyeExplorateur.rotateUp(posMouse.y);
                        }
                    }
                }

                break;

            default:
                break;
            }
        }

        /************** DESSIN *******************/

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 globalMVMatrix;
        if (cameraTrackExplorateur.getIsActive())
        {
            globalMVMatrix = cameraTrackExplorateur.getViewMatrix();
        }
        else
        {
            globalMVMatrix = cameraEyeExplorateur.getViewMatrix(player);
        }

        // Dessin de la skybox
        skybox.draw(projMatrix, globalMVMatrix);

        // Dessin du menu

        glm::mat4 menuMVMatrix;
        menuMVMatrix = glm::translate(globalMVMatrix, glm::vec3(0, 3, 1.5));
        menuMVMatrix = glm::rotate(menuMVMatrix, -30.0f, glm::vec3(1, 0, 0));
        menuMVMatrix = glm::scale(menuMVMatrix, glm::vec3(2.5, 2.5, 1));

        if (!game || pause || gameOver)
        {
            if (!game)
            {
                menuDrawing.draw(menuTexture, menuMVMatrix, projMatrix, windowManager.getTime());
            }
            else if (pause)
            {
                menuDrawing.draw(pauseTexture, menuMVMatrix, projMatrix, windowManager.getTime());
            }
            else if (cameraTrackExplorateur.getIsActive())
            {
                cameraTrackExplorateur.disactive();
                cameraEyeExplorateur.getIsActive();
            }
            menuDrawing.draw(gameOverTexture, menuMVMatrix, projMatrix, windowManager.getTime());
        }
        // Dessin du jeu
        else
        {
            // Dessin des personnages
            playerDrawing.drawPlayer(playerTexture, globalMVMatrix, projMatrix, player, windowManager.getTime());
            playerDrawing.drawEnemy(enemyTexture, globalMVMatrix, projMatrix, player, windowManager.getTime());

            // Dessin du terrain
            globalMVMatrix = glm::translate(globalMVMatrix, glm::vec3(-1, advancement, 0)); 

            //Génération du sol, des obstacles et des barrières
            drawMap(vecTerrain, globalMVMatrix, floorDrawing, projMatrix, floorTexture, tileTexture, windowManager.getTime(), limitTexture);

            //Generation des pieces
            drawCoins(vecTerrain, globalMVMatrix, coinDrawing, projMatrix, coinTexture, windowManager.getTime());

            // Update, on s'arrête s'il y a collision, chûte ou qu'on arrive à la fin du terrain
            if (!faceCollision(player, vecTerrain) && !fall(player, vecTerrain) && player.getPosition().y < vecTerrain.size() / 3)
            {
                advancement -= 0.1f;
                actionDuration = player.endMovement(actionDuration);
                // Si on atteint une nouvelle case, on met à jour la position du joueur
                player.caseAdvancement(advancement);
                unsigned int valeurPiece = vecTerrain.getPieceValue(player.getPosition().y, player.getPosition().x);
                if (valeurPiece)
                {
                    vecTerrain.deletePiece(player.getPosition().y, player.getPosition().x);
                    player.increaseScore(valeurPiece);
                }
            }

            // Affichage du score final
            else
            {
                std::cout << "SCORE FINAL : " << player.getNbPoints() << std::endl;
                player.start();
                advancement = 0;
                gameOver = true;
            }
        }

        glBindVertexArray(0);

        /************** FIN DESSIN *****************/

        // Update the display
        windowManager.swapBuffers();
    }

    return EXIT_SUCCESS;
}
