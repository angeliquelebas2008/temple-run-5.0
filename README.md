# Templeris

Les librairies nécessaires pour le projet sont : SDL, OpenGL, Glew, SDL_ttf.
Pour compiler notre jeu, nous utilisons Cmake. Dans le répertoire source, il suffit de saisir “bash compile.sh” dans un terminal.

Commandes de jeu:

Touche Echap : bouton pause
Touche Space : commencer la partie
Touche Q : déplacement à gauche
Touche D : déplacement à droite
Touche Z : saut du personnage
Touche S : glissade pour se baisser 
Touche R : recommencer la partie après une défaite ou une pause
Touche C : passage de la caméra en vue 1ere personne et 3ème personne et vice versa
Touche L : réinitialisation de la position de la caméra (position optimale)
Flèches haut et bas : déplacement des caméras
Clic gauche: rotation de la caméra en vue 3eme personne
Clic droit: rotation de la caméra en vue 1ere personne